package employee

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
)

type Employee struct {
	Id   string `json:"id"`
	Name     string    `json:"name"`
	Gender string `json:"gender"`
	IsAvailable bool    `json:"isAvailable"`
}

var ErrNoEmployee = errors.New("No find employee")

func GetEmployees () ([]byte, error) {
	data, err := ioutil.ReadFile("./data/data.json")
	if err != nil {
		return nil, err
	}
	return data, nil
}

func AddEmployee (employee Employee) error {
	var employees[]Employee
	data, err := ioutil.ReadFile("./data/data.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &employees)
	if  err != nil {
		return err
	}
	employees = append(employees, employee)
	updatedData, err := json.Marshal(employees)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile("./data/data.json", updatedData, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func GetEmployee(id string) (Employee, error) {
	var employees[]Employee
	data, err := ioutil.ReadFile("./data/data.json")
	if err != nil {
		return Employee{}, err
	}
	err = json.Unmarshal(data, &employees)
	if  err != nil {
		return Employee{}, err
	}
	for  index := 0; index < len(employees); index++ {
		if employees[index].Id == id {
			return employees[index], nil
		}
	}
	return Employee{}, ErrNoEmployee
}

func DeleteEmployee(id string) error {
	var employees[]Employee
	data, err := ioutil.ReadFile("./data/data.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &employees)
	if err != nil {
		return err
	}
	for index := 0; index < len(employees); index++ {
		if employees[index].Id == id {
			employees = removeObject(employees, index)
			updatedData, err := json.Marshal(employees)
			if err !=  nil {
				return nil
			}
			err = ioutil.WriteFile("./data/data.json", updatedData, os.ModePerm)
			if err != nil {
				return err
			}
			return nil
		}
	}
	return ErrNoEmployee
}

func removeObject(arr[]Employee, index int) []Employee{
	ret := make([]Employee, 0)
	ret = append(ret, arr[:index]...)
	return append(ret, arr[index+1:]...)
}







