package  handlers

import (
	"crypto/sha256"
	"crypto/subtle"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/API/employee"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"os"
)
func GetEmployeesHandler() http.HandlerFunc {
	return func(rw http.ResponseWriter, r  *http.Request) {
		data, err := employee.GetEmployees()
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		rw.Header().Add("content-type", "application/json")
		rw.WriteHeader(http.StatusOK)
		rw.Write(data)
	}
}

func CreateEmployeeHandler() http.HandlerFunc {
	return func(rw http.ResponseWriter, r  *http.Request) {
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		var info employee.Employee
		err = json.Unmarshal(data, &info)
		if err != nil {
			fmt.Println(err)
			rw.WriteHeader(http.StatusExpectationFailed)
			rw.Write([]byte("Invalid Data Format"))
			return
		}
		err = employee.AddEmployee(info)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		rw.WriteHeader(http.StatusCreated)
		rw.Write([]byte("Added new JSON succeed "))
	}
}

func GetEmployeeHandler() http.HandlerFunc {
	return func(rw  http.ResponseWriter, r *http.Request) {
		employeeID := mux.Vars(r) ["id"]
		EmployeeInfo, err := employee.GetEmployee(employeeID)
		if err != nil {
			fmt.Println(err)
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		responseData, err :=  json.Marshal(EmployeeInfo)
		if err != nil {
			if errors.Is(err, employee.ErrNoEmployee) {
				rw.WriteHeader(http.StatusNoContent)
			} else {
				rw.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		rw.Header().Add("content-type", "application/json")
		rw.WriteHeader(http.StatusFound)
		rw.Write(responseData)
	}
}

func DeleteEmployeeHandler() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		employeeID := mux.Vars(r) ["id"]
		err := employee.DeleteEmployee(employeeID)
		if err != nil {
			if errors.Is(err, employee.ErrNoEmployee) {
				rw.WriteHeader(http.StatusNoContent)
			} else {
				rw.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		rw.WriteHeader(http.StatusAccepted)
	}
}

func UpdateEmployeeHandler() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		employeeID := mux.Vars(r) ["id"]
		err := employee.DeleteEmployee(employeeID)
		if err != nil {
			if errors.Is(err, employee.ErrNoEmployee) {
				rw.WriteHeader(http.StatusNoContent)
			} else {
				rw.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		var NewEmployee employee.Employee
		err = json.Unmarshal(data, &NewEmployee)
		if err != nil {
			fmt.Println(err)
			rw.WriteHeader(http.StatusExpectationFailed)
			rw.Write([]byte("Invalid Data Format"))
			return
		}
		err = employee.AddEmployee(NewEmployee)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		rw.WriteHeader(http.StatusAccepted)

	}
}

func AuthHandler(h http.Handler) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if ok {
			username := sha256.Sum256([]byte(os.Getenv("User")))
			password := sha256.Sum256([]byte(os.Getenv("Password")))
			userHash := sha256.Sum256([]byte(user))
			passHash := sha256.Sum256([]byte(pass))
			validUser := subtle.ConstantTimeCompare(userHash[:],username[:]) == 1
			validPass := subtle.ConstantTimeCompare(passHash[:],password[:]) == 1
			if validUser && validPass {
				h.ServeHTTP(rw, r)
				return
			}
		}
		http.Error(rw, "Invalid Credentials", http.StatusUnauthorized)
	}
}

