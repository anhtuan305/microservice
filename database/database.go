package database

import (
	"database/sql"
	"fmt"
	_"github.com/lib/pq"
	"strconv"
)


func ConnectDatabase (user, password, host, dbname, dbPort, ssmode string) (*sql.DB, error){
    port, _ := strconv.Atoi(dbPort)
    pass, _ := strconv.Atoi(password)
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%d dbname=%s sslmode=%s", host, port,user,pass, dbname, ssmode)
	var err error
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return nil, err
}
