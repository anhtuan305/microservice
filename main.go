package main

import (
	"database/sql"
	"fmt"
	"github.com/API/database"
	"github.com/API/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"net/http"
	"os"
)

var db *sql.DB

func main() {
	os.Setenv("User", "anhtuan305")
	os.Setenv("Password", "30051998")
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Error loading .env file")
	}
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbSSLMode := os.Getenv("DB_SSLMODE")
	dbPort := os.Getenv("DB_PORT")
	dbHost := os.Getenv("DB_HOST")
	db,err = database.ConnectDatabase(dbUser, dbPassword,dbHost, dbName,dbPort, dbSSLMode)
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected")

	router := mux.NewRouter()
	router.Handle("/employee", handlers.GetEmployeesHandler()).Methods("GET")
	router.Handle("/employee", handlers.CreateEmployeeHandler()).Methods("POST")
	router.Handle("/employee/{id}", handlers.GetEmployeeHandler()).Methods("GET")
	router.Handle("/employee/{id}", handlers.UpdateEmployeeHandler()).Methods("PUT")
	router.Handle("/employee/{id}", handlers.DeleteEmployeeHandler()).Methods("DELETE")
	server := http.Server{
		Addr:    ":9090",
		Handler: handlers.AuthHandler(router),
	}

	fmt.Println("Server on Port 9090")
	server.ListenAndServe()

}




